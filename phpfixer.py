import datetime
import os
import re
import subprocess
import threading
import time
import sublime
import sublime_plugin
import sys

try:
    from HTMLParser import HTMLParser
except:
    from html.parser import HTMLParser
from os.path import expanduser

class Pref:

    project_file = None

    keys = [
        "show_debug",
        "extensions_to_execute",
        "extensions_to_blacklist",
        "phpfixer_execute_on_save",
        "phpfixer_on_save",
        "phpfixer_show_quick_panel",
        "phpfixer_executable_path",
        "phpfixer_additional_args",
        "phpfixer_linter_run",
        "phpfixer_linter_command_on_save",
        "phpfixer_php_path",
        "phpfixer_linter_regex"
    ]

    def load(self):
        self.settings = sublime.load_settings('phpfixer.sublime-settings')

        if sublime.active_window() is not None and sublime.active_window().active_view() is not None:
            project_settings = sublime.active_window().active_view().settings()
            if project_settings.has("phpfixer"):
                project_settings.clear_on_change('phpfixer')
                self.project_settings = project_settings.get('phpfixer')
                project_settings.add_on_change('phpfixer', pref.load)
            else:
                self.project_settings = {}
        else:
            self.project_settings = {}

        for key in self.keys:
            self.settings.clear_on_change(key)
            setattr(self, key, self.get_setting(key))
            self.settings.add_on_change(key, pref.load)

    def get_setting(self, key):
        if key in self.project_settings:
            return self.project_settings.get(key)
        else:
            return self.settings.get(key)

    def set_setting(self, key, value):
        if key in self.project_settings:
            self.project_settings[key] = value
        else:
            self.settings.set(key, value)


pref = Pref()

st_version = 3

def plugin_loaded():
    pref.load()

def debug_message(msg):
    if pref.show_debug == True:
        print("[phpfixer] " + str(msg))


class CheckstyleError():
    """Represents an error that needs to be displayed on the UI for the user"""
    def __init__(self, line, message):
        self.line = line
        self.message = message

    def get_line(self):
        return self.line

    def get_message(self):
        data = self.message

        if st_version == 3:
            return HTMLParser().unescape(data)
        else:
            try:
                data = data.decode('utf-8')
            except UnicodeDecodeError:
                data = data.decode(sublime.active_window().active_view().settings().get('fallback_encoding'))
            return HTMLParser().unescape(data)

    def set_point(self, point):
        self.point = point

    def get_point(self):
        return self.point


class ShellCommand():
    """Base class for shelling out a command to the terminal"""
    def __init__(self):
        self.error_list = []

        # Default the working directory for the shell command to the user's home dir.
        self.workingDir = expanduser("~")

    def setWorkingDir(self, dir):
        self.workingDir = dir

    def get_errors(self, path):
        self.execute(path)
        return self.error_list

    def shell_out(self, cmd):
        data = None
        
        for i, arg in enumerate(cmd):
            if isinstance(arg, str) and arg.startswith('~'):
                cmd[i] = os.path.expanduser(arg)

        if st_version == 3:
            debug_message(' '.join(cmd))
        else:
            for index, arg in enumerate(cmd[:]):
                cmd[index] = arg.encode(sys.getfilesystemencoding())

            debug_message(' '.join(cmd))

        debug_message(' '.join(cmd))

        info = None
        if os.name == 'nt':
            info = subprocess.STARTUPINFO()
            info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            info.wShowWindow = subprocess.SW_HIDE

        debug_message("cwd: " + self.workingDir)
        proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=info, cwd=self.workingDir)


        if proc.stdout:
            data = proc.communicate()[0]

        return data.decode()

    def execute(self, path):
        debug_message('Command not implemented')

class Fixer(ShellCommand):
    """Concrete class for PHP-CS-Fixer"""
    def execute(self, path):

        args = []


        if pref.phpfixer_executable_path != "":
            if (len(args) > 0):
                args.append(pref.phpfixer_executable_path)
            else:
                args = [pref.phpfixer_executable_path]
        else:
            debug_message("phpfixer_executable_path is not set, therefore cannot execute")
            sublime.error_message('The "phpfixer_executable_path" is not set, therefore cannot execute this command')
            return

        args.append("fix")
        args.append(os.path.normpath(path))
        args.append("--verbose")

        # Add the additional arguments from the settings file to the command
        for key, value in pref.phpfixer_additional_args.items():
            arg = key
            if value != "":
                arg += "=" + value
            args.append(arg)

        self.parse_report(args)

    def parse_report(self, args):
        report = self.shell_out(args)
        debug_message(report)
        lines = re.finditer('.*(?P<line>\d+)\) (?P<file>.*)', report)

        for line in lines:
            error = CheckstyleError(line.group('line'), line.group('file'))
            self.error_list.append(error)

class Linter(ShellCommand):
    """Content class for php -l"""
    def execute(self, path):
        if pref.phpfixer_linter_run != True:
            return

        if pref.phpfixer_php_path != "":
            args = [pref.phpfixer_php_path]
        else:
            args = ['php']

        args.append("-l")
        args.append("-d display_errors=On")
        args.append(os.path.normpath(path))

        self.parse_report(args)

    def parse_report(self, args):
        report = self.shell_out(args)
        debug_message(report)
        line = re.search(pref.phpfixer_linter_regex, report)
        if line != None:
            error = CheckstyleError(line.group('line'), line.group('message'))
            self.error_list.append(error)


class phpfixerCommand():
    """Main plugin class for building the checkstyle report"""

    # Class variable, stores the instances.
    instances = {}

    @staticmethod
    def instance(view, allow_new=True):
        '''Return the last-used instance for a given view.'''
        view_id = view.id()
        if view_id not in phpfixerCommand.instances:
            if not allow_new:
                return False
            phpfixerCommand.instances[view_id] = phpfixerCommand(view)
        return phpfixerCommand.instances[view_id]

    def __init__(self, view):
        self.view = view
        self.checkstyle_reports = []
        self.report = []
        self.event = None
        self.error_lines = {}
        self.error_list = []
        self.shell_commands = ['Linter', 'Sniffer', 'MessDetector']
        self.standards = []

    def run(self, path, event=None):
        self.event = event
        self.checkstyle_reports = []
        self.report = []

        if event != 'on_save':
            if pref.phpfixer_linter_run:
                self.checkstyle_reports.append(['Linter', Linter().get_errors(path), 'dot'])
        else:
            if pref.phpfixer_linter_command_on_save and pref.phpfixer_linter_run:
                self.checkstyle_reports.append(['Linter', Linter().get_errors(path), 'dot'])

    def fix_standards_errors(self, tool, path):
        self.error_lines = {}
        self.error_list = []
        self.report = []

        fixes = Fixer().get_errors(path)

        for fix in fixes:
            self.error_list.append(fix.get_message())



class phpfixerTextBase(sublime_plugin.TextCommand):
    """Base class for Text commands in the plugin, mainly here to check php files"""
    description = ''

    def run(self, args):
        debug_message('Not implemented')

    def description(self):
        if not phpfixerTextBase.should_execute(self.view):
            return "Invalid file format"
        else:
            return self.description

    @staticmethod
    def should_execute(view):
        if view.file_name() != None:

            try:
                ext = os.path.splitext(view.file_name())[1]
                result = ext[1:] in pref.extensions_to_execute
            except:
                debug_message("Is 'extensions_to_execute' setup correctly")
                return False

            for block in pref.extensions_to_blacklist:
                match = re.search(block, view.file_name())
                if match != None:
                    return False

            return result

        return False



class phpfixerFixThisFileCommand(phpfixerTextBase):
    """Command to use php-cs-fixer to 'fix' the file"""
    description = 'Fix coding standard issues (php-cs-fixer)'

    def run(self, args, tool="Fixer"):
        debug_message(tool)
        cmd = phpfixerCommand.instance(self.view)
        cmd.fix_standards_errors(tool, self.view.file_name())

    def is_enabled(self):
        return phpfixerTextBase.should_execute(self.view)


class phpfixerFixThisDirectoryCommand(sublime_plugin.WindowCommand):
    """Command to use php-cs-fixer to 'fix' the directory"""
    def run(self, tool="Fixer", paths=[]):
        cmd = phpfixerCommand.instance(self.window.active_view())
        cmd.fix_standards_errors(tool, os.path.normpath(paths[0]))

    def is_enabled(self):
        if pref.phpfixer_executable_path != '':
            return True
        else:
            return False

    def is_visible(self, paths=[]):
        return True

    def description(self, paths=[]):
        return 'Fix this directory (PHP-CS-Fixer)'


class phpfixerTogglePlugin(phpfixerTextBase):
    """Command to toggle if plugin should execute on save"""
    def run(self, edit, toggle=None):
        if toggle == None:
            if pref.phpfixer_execute_on_save == True:
                pref.phpfixer_execute_on_save = False
            else:
                pref.phpfixer_execute_on_save = True
        else:
            if toggle :
                pref.phpfixer_execute_on_save = True
            else:
                pref.phpfixer_execute_on_save = False

    def is_enabled(self):
        return phpfixerTextBase.should_execute(self.view)

    def description(self, paths=[]):
        if pref.phpfixer_execute_on_save == True:
            description = 'Turn Execute On Save Off'
        else:
            description = 'Turn Execute On Save On'
        return description


class phpfixerEventListener(sublime_plugin.EventListener):
    """Event listener for the plugin"""
    def on_post_save(self, view):
        if phpfixerTextBase.should_execute(view):
            if pref.phpfixer_execute_on_save == True:
                cmd = phpfixerCommand.instance(view)
                thread = threading.Thread(target=cmd.run, args=(view.file_name(), 'on_save'))
                thread.start()

            if pref.phpfixer_execute_on_save == True and pref.phpfixer_on_save == True:
                cmd = phpfixerCommand.instance(view)
                cmd.fix_standards_errors("Fixer", view.file_name())

    def on_pre_save(self, view):
        """ Project based settings, currently able to see an API based way of doing this! """
        if not phpfixerTextBase.should_execute(view) or st_version == 2:
            return

        current_project_file = view.window().project_file_name();
        debug_message('Project files:')
        debug_message(' Current: ' + str(current_project_file))
        debug_message(' Last Known: ' + str(pref.project_file))

        if current_project_file == None:
            debug_message('No project file defined, therefore skipping reload')
            return

        if pref.project_file == current_project_file:
            debug_message('Project files are the same, skipping reload')
        else:
            debug_message('Project files have changed, commence the reload')
            pref.load();
            pref.project_file = current_project_file
